﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Pool
{
    public abstract class ObjectPool<T> : MonoBehaviour, IPool<T> where T : MonoBehaviour
    {
        [SerializeField] private int poolLimit;
        private readonly List<T> _objectPool = new List<T>();
        private T _objectPrefab;

        private int _index;

        public void Init(T objectPrefab)
        {
            _objectPrefab = objectPrefab;
            _objectPool.Clear();
        }

        public void Prewarm(int init)
        {
            for (var i = 0; i < init; i++) Create();
        }

        public T Request()
        {
            foreach (var obj in _objectPool.Where(obj => !obj.gameObject.activeSelf))
            {
                obj.gameObject.SetActive(true);
                return obj;
            }

            if (poolLimit > 0 && _objectPool.Count >= poolLimit)
            {
                return _objectPool[_index++ % _objectPool.Count];
            }

            return Create();
        }

        public void Return(T member)
        {
            member.gameObject.SetActive(false);
        }

        private T Create()
        {
            var obj = Instantiate(_objectPrefab, transform);
            obj.gameObject.SetActive(false);
            _objectPool.Add(obj);
            return obj;
        }
    }
}