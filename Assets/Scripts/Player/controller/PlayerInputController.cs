﻿using System;
using Core.events;
using Core.spectator;
using UnityEngine;

namespace Player.controller
{
    public class PlayerInputController : MonoBehaviour
    {
        private PlayerInput _playerInput;

        private void Awake()
        {
            _playerInput = FindObjectOfType<PlayerInput>();
        }

        private void OnEnable()
        {
            EventInstance.Instance.PlayerEvent.PlayerSpectateEvent += OnPlayerSpectateEvent;
        }

        private void OnDisable()
        {
            EventInstance.Instance.PlayerEvent.PlayerSpectateEvent -= OnPlayerSpectateEvent;
        }

        private void OnPlayerSpectateEvent(ISpectator spectator)
        {
            _playerInput.enabled = spectator is Core.entity.Player;
        }
    }
}