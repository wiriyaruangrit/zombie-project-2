﻿using System.raycast;
using UnityEngine;

namespace Player.raycast
{
    public class CameraRayProvider : MonoBehaviour, IRayProvider
    {
        private Camera _camera;

        private void Awake()
        {
            _camera = Camera.main;
        }

        public Ray GetRay()
        {
            return _camera.ScreenPointToRay(Input.mousePosition);
        }
    }
}