﻿using System.raycast;
using UnityEngine;

namespace Player.raycast
{
    public class PlayerRaycastProvider : MonoBehaviour, IRayCastProvider
    {
        [SerializeField] private float maxDistance;
        private IRayCastHitResponse _rayCastHitResponse;
        private IRayProvider _rayProvider;

        private void Awake()
        {
            _rayProvider = GetComponent<IRayProvider>();
            _rayCastHitResponse = GetComponent<IRayCastHitResponse>();
        }

        private void FixedUpdate()
        {
            Physics.Raycast(_rayProvider.GetRay(), out var hit, maxDistance);
            _rayCastHitResponse.OnRaycastHit(hit);
        }

        public IRayCastHitResponse GetRayCastHitResponse()
        {
            return _rayCastHitResponse;
        }
    }
}