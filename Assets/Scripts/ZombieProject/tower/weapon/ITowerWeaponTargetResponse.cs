﻿using Core.entity;

namespace ZombieProject.tower.weapon
{
    public interface ITowerWeaponTargetResponse
    {
        void OnTarget(LivingEntity target);
    }
}