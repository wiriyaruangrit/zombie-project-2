﻿using UnityEngine;

namespace ZombieProject.tower
{
    [CreateAssetMenu(menuName = "TD/Tower")]
    public class TowerSo : ScriptableObject
    {
        [Header("ITEM ICON")] public EnumValue enumValue;
        [Header("TOWER")] public Tower towerPrefab;
        [Space] public string displayName;
        public string[] description;
        public bool forSell;
        public int price;
        [Header("BUILD TOWER")] public bool instantBuild;
        [Header("UPGRADE TOWER")] public TowerSo[] upgradeTowers;
    }
}