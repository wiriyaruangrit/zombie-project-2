﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ZombieProject.events;

namespace ZombieProject.game.wave
{
    public class Wave
    {
        private readonly float _timerUpdateThreshold;
        private readonly IWaveManager _waveManager;
        private readonly IWavePath _wavePath;

        private readonly WaveSo _waveSo;
        private readonly IWaveSpawner _waveSpawner;
        private float _currentUpdateThreshold;
        private bool _isWaveOver;
        private List<SpawnData> _waveEvent;

        private float _waveTimer;

        public WaveSo GetWaveSo()
        {
            return _waveSo;
        }

        public Wave(WaveSo waveSo, IWaveManager waveManager, IWaveSpawner waveSpawner, IWavePath wavePath,
            float timerUpdateThreshold)
        {
            _waveSo = waveSo;
            _waveManager = waveManager;
            _waveSpawner = waveSpawner;
            _wavePath = wavePath;
            _timerUpdateThreshold = timerUpdateThreshold;
        }

        public void StartWave()
        {
            InitSpawnData();
            _waveTimer = 0f;
        }

        public void UpdateWave()
        {
            if (_isWaveOver) return;

            _currentUpdateThreshold -= Time.deltaTime;
            if (_currentUpdateThreshold < 0)
            {
                _waveTimer += _timerUpdateThreshold;
                _currentUpdateThreshold = _timerUpdateThreshold;
                GameEventInstance.Instance.GameEvent.OnWaveTimerTickEvent(_waveTimer, _waveSo.waveDelay);
            }

            if (_waveTimer > _waveSo.waveDelay)
            {
                _isWaveOver = true;
                GameEventInstance.Instance.GameEvent.OnWaveEndedEvent(_waveManager);
            }

            for (var i = 0; i < _waveEvent.Count; i++)
            {
                var data = _waveEvent[i];
                if (_waveTimer < data.EventTime) continue;
                _waveEvent.Remove(data);
                SpawnEnemy(data.WaveData);
            }
        }

        private void InitSpawnData()
        {
            _waveEvent = new List<SpawnData>();

            foreach (var waveEvent in _waveSo.wavesEvents)
            {
                var eventTime = waveEvent.eventDelay;
                for (var i = 0; i < waveEvent.spawnCount; i++)
                {
                    _waveEvent.Add(new SpawnData(waveEvent, eventTime));
                    eventTime += waveEvent.spawnDelay;
                }
            }
        }

        private void SpawnEnemy(WaveSo.WaveEventData waveEventData)
        {
            var walkPath = (PathType[]) Enum.GetValues(typeof(PathType));
            var randomPathType = walkPath[UnityEngine.Random.Range(0, walkPath.Length)];
            var enemyPath = _wavePath.GetStartPoint(waveEventData.entranceType, randomPathType);
            if (!enemyPath)
            {
                Debug.LogWarning($"No checkpoint found: {waveEventData.entranceType}");
                return;
            }

            _waveSpawner.SpawnEnemy(waveEventData.entityType, enemyPath);

            if (_waveEvent.Count < 1) GameEventInstance.Instance.GameEvent.OnWaveSpawnCompletedEvent();
        }

        private readonly struct SpawnData
        {
            public readonly WaveSo.WaveEventData WaveData;
            public readonly float EventTime;

            public SpawnData(WaveSo.WaveEventData waveData, float eventTime)
            {
                WaveData = waveData;
                EventTime = eventTime;
            }
        }
    }
}