﻿using System;
using Core.entity;
using UnityEngine;

namespace ZombieProject.game.wave.mobbounty
{
    [CreateAssetMenu(menuName = "TD/Mob Bounty")]
    public class MobBountySO : ScriptableObject
    {
        [SerializeField] private MobBountyData[] mobBountyData;

        public MobBountyData[] MobBountyRewardTable => mobBountyData;

        [Serializable]
        public struct MobBountyData
        {
            public EntityType entityType;
            public int reward;
        }
    }
}