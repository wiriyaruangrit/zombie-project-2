﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ZombieProject.game.checkpoint;

namespace ZombieProject.game.wave
{
    public class WavePath : MonoBehaviour, IWavePath
    {
        [SerializeField] private PathData[] pathData;

        private Dictionary<string, CheckPoint> _checkPoints;

        private void Awake()
        {
            InitPath();
        }

        public CheckPoint GetStartPoint(EntranceType entranceType, PathType pathType)
        {
            var primaryKey = GetPrimaryKey(entranceType, pathType);
            return !_checkPoints.ContainsKey(primaryKey) ? null : _checkPoints[primaryKey];
        }

        private void InitPath()
        {
            _checkPoints = new Dictionary<string, CheckPoint>();
            foreach (var data in pathData)
                _checkPoints.Add(GetPrimaryKey(data.entranceType, data.pathType), data.startPoint);
        }

        private string GetPrimaryKey(EntranceType entranceType, PathType pathType)
        {
            return $"{entranceType}{pathType}";
        }

        [Serializable]
        public struct PathData
        {
            public EntranceType entranceType;
            public PathType pathType;
            public CheckPoint startPoint;
        }
    }
}