﻿using System;
using Core.entity;
using UnityEngine;

namespace ZombieProject.game.wave
{
    [CreateAssetMenu(menuName = "TD/WaveSo", order = 0)]
    public class WaveSo : ScriptableObject
    {
        public float waveDelay;
        public WaveEventData[] wavesEvents;

        [Serializable]
        public struct WaveEventData
        {
            public float eventDelay;
            [Header("WAVE EVENT")] public float spawnDelay;

            public EntranceType entranceType;

            //public PathType walkPathType;
            public EntityType entityType;
            public int spawnCount;
        }
    }
}