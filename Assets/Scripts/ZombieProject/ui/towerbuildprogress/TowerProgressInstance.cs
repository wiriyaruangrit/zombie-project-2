﻿using System;
using System.Collections.Generic;
using Singleton;
using UnityEngine;
using ZombieProject.tower;

namespace ZombieProject.ui.towerbuildprogress
{
    public class TowerProgressInstance : ResourceSingleton<TowerProgressInstance>
    {
        [SerializeField] private TowerProgressData[] towerProgressData;
        private Dictionary<TowerProgressBarType, TowerProgressUI> _progressUi;

        public override void Awake()
        {
            base.Awake();
            InitHealthBar();
        }

        private void InitHealthBar()
        {
            _progressUi = new Dictionary<TowerProgressBarType, TowerProgressUI>();
            foreach (var data in towerProgressData)
            {
                if (_progressUi.ContainsKey(data.towerProgressBarType)) continue;
                _progressUi.Add(data.towerProgressBarType, data.progressUI);
            }
        }

        public static TowerProgressUI SetTowerProgress(Tower tower, TowerProgressBarType towerProgressBarType)
        {
            var towerProgressUI = Instantiate(Instance._progressUi[towerProgressBarType]);
            towerProgressUI.Init(tower);
            return towerProgressUI;
        }

        [Serializable]
        public struct TowerProgressData
        {
            public TowerProgressBarType towerProgressBarType;
            public TowerProgressUI progressUI;
        }
    }
}