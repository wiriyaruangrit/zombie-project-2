﻿namespace ZombieProject.ui.gameresult
{
    public interface IVictoryScreen : IVisible
    {
        void SetReward(int points);
    }
}