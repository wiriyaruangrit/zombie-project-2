﻿using TMPro;
using UnityEngine;

namespace ZombieProject.ui.healthpoint
{
    public class HealthPointDisplayDisplay : MonoBehaviour, IHealthPointDisplay

    {
        [SerializeField] private TextMeshProUGUI healthPointText;

        public void SetHealthPoint(int previous, int current)
        {
            healthPointText.text = $"{current}";
        }
    }
}