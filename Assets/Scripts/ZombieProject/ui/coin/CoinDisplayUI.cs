﻿using Core.ui;
using UnityEngine;
using ZombieProject.events;
using ZombieProject.game.wave;

namespace ZombieProject.ui.coin
{
    public class CoinDisplayUI : BaseUI
    {
        [SerializeField] private GameEventSo gameEventSo;
        private ICoinDisplay _coinDisplay;

        public override void Awake()
        {
            base.Awake();
            _coinDisplay = GetComponent<ICoinDisplay>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            gameEventSo.CoinChangeEvent += OnCoinChangeEvent;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            gameEventSo.CoinChangeEvent -= OnCoinChangeEvent;
        }

        private void OnCoinChangeEvent(IWaveManager waveManager, int previous, int current)
        {
            _coinDisplay.SetCoin(previous, current);
        }
    }
}