﻿namespace ZombieProject.ui.playerrespawn
{
    public interface IPlayerRespawnTimer : IVisible
    {
        public void SetRespawnTimer(float current, float delay);
    }
}