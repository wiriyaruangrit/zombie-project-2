﻿using Core.projectile;
using UnityEngine;

namespace ZombieProject.weapon
{
    [CreateAssetMenu(menuName = "TD/Weapon")]
    public class WeaponSo : ScriptableObject
    {
        [Header("ITEM ICON")] public EnumValue enumValue;
        [Header("WEAPON SETTINGS")] public WeaponType weaponType;
        public ProjectileType projectileType;
        [Space] public string displayName;
        public string[] description;
        public bool isAuto;
        public float minDamage;
        public float maxDamage;
        public float speed;
        public float fireRate;
        public int maxAmmo;
        public float reloadTime;
        public float range;

        [Header("SHOP")] public int price;
    }
}