﻿using System;
using UnityEngine;

namespace Core.projectile.explode
{
    [RequireComponent(typeof(Rigidbody))]
    public class Explode : MonoBehaviour
    {
        [SerializeField] private LeanTweenType explodeTweenType;

        private Action<Collider> _action;

        public void Init(float radius, float delay, Action<Collider> action)
        {
            _action = action;

            LeanTween.scale(gameObject, new Vector3(radius, radius, radius), delay)
                .setEase(explodeTweenType)
                .setOnComplete(() => { Destroy(gameObject); });
        }

        private void OnTriggerStay(Collider other)
        {
            _action(other);
        }
    }
}