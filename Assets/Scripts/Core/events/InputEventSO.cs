﻿using System;
using UnityEngine;

namespace Core.events
{
    [CreateAssetMenu(menuName = "Event/Input Event Channel")]
    public class InputEventSO : ScriptableObject
    {
        //INTERACT
        public event Action<bool> AttackEvent;
        public event Action<bool> UseItemEvent;


        public void OnAttackEvent(bool mouseDown)
        {
            AttackEvent?.Invoke(mouseDown);
        }

        public void OnUseItemEvent(bool mouseDown)
        {
            UseItemEvent?.Invoke(mouseDown);
        }

        //INVENTORY
        public event Action<bool> PickItemEvent;
        public event Action InventoryEvent;

        public void OnPickItemEvent(bool wheelMouseDown)
        {
            PickItemEvent?.Invoke(wheelMouseDown);
        }

        public void OnInventoryEvent()
        {
            InventoryEvent?.Invoke();
        }

        //MOVEMENT
        public event Action<float> MoveVerticalEvent;
        public event Action<float> MoveHorizontalEvent;
        public event Action JumpEvent;
        public event Action SneakEvent;

        public void OnMoveVertical(float speed)
        {
            MoveVerticalEvent?.Invoke(speed);
        }

        public void OnMoveHorizontal(float speed)
        {
            MoveHorizontalEvent?.Invoke(speed);
        }

        public void OnJumpEvent()
        {
            JumpEvent?.Invoke();
        }

        public void OnSneakEvent()
        {
            SneakEvent?.Invoke();
        }

        //UI
        public event Action ToggleUIEvent;
        public event Action ToggleMenuEvent;

        public void OnToggleUIEvent()
        {
            ToggleUIEvent?.Invoke();
        }

        public void OnToggleMenuEvent()
        {
            ToggleMenuEvent?.Invoke();
        }
    }
}