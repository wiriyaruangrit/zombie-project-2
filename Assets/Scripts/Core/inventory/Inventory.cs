﻿using System;
using System.Linq;

namespace Core.inventory
{
    public class Inventory : IInventory
    {
        private readonly InventoryType _inventoryType;
        private ItemStack[] _items;

        public Inventory(int size, InventoryType inventoryType)
        {
            _inventoryType = inventoryType;
            _items = new ItemStack[size];
        }

        public virtual void SetItem(int index, ItemStack itemStack)
        {
            _items[index] = itemStack;
        }

        public virtual void AddItem(ItemStack itemStack)
        {
            _items[FirstEmpty()] = itemStack;
        }

        public virtual void Remove(ItemStack itemStack)
        {
            for (var i = 0; i < _items.Length; i++)
            {
                var item = _items[i];
                if (item != itemStack) continue;
                _items[i] = null;
            }
        }

        public bool HasItem(ItemStack itemStack)
        {
            return _items.Any(item => item == itemStack);
        }

        public int FirstEmpty()
        {
            for (var i = 0; i < _items.Length; i++)
            {
                var item = _items[i];
                if (item == null) return i;
            }

            return -1;
        }

        public ItemStack[] GetContents()
        {
            //TODO: Clone Item
            return _items;
        }

        public virtual void SetStorageContents(ItemStack[] items)
        {
            _items = items;
        }

        public ItemStack GetItem(int index)
        {
            return _items[index];
        }

        public int GetSize()
        {
            return _items.Length;
        }

        public new InventoryType GetType()
        {
            return _inventoryType;
        }

        public bool IsEmpty()
        {
            return _items.Length == 0;
        }
    }
}