﻿using UnityEngine;

namespace Core.inventory.InventoryView.InventoryViewProvider
{
    public class DefaultInventoryViewProvider : InventoryViewProvider
    {
        [SerializeField] private ItemSlot itemSlotTemplate;
        [SerializeField] private Transform container;

        public override void Init(InventoryView inventoryView)
        {
            foreach (Transform item in container) Destroy(item.gameObject);

            itemSlotTemplate.gameObject.SetActive(true);
            var items = inventoryView.GetInventory().GetContents();

            for (var i = 0; i < items.Length; i++)
            {
                var item = items[i];
                if (item == null) continue;
                var itemSlot = Instantiate(itemSlotTemplate, container);
                itemSlot.Init(item, i, inventoryView);
            }

            itemSlotTemplate.gameObject.SetActive(false);
        }
    }
}