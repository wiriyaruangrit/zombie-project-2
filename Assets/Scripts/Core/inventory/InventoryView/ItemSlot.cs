﻿using Core.events;
using ScriptableObjects.Inventory;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Core.inventory.InventoryView
{
    public class ItemSlot : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private InventoryEventSO inventoryEventSo;
        [SerializeField] private ItemIconPack itemIconPack;
        [SerializeField] private Image slotIcon;
        [SerializeField] private Image hoverOverlay;
        [SerializeField] private TextMeshProUGUI itemAmountText;
        private InventoryView _inventoryView;

        private ItemStack _itemStack;
        private int _slot;

        public void OnPointerClick(PointerEventData eventData)
        {
            _inventoryView.SetAction(GetAction(_itemStack));
            _inventoryView.SetClickType(GetClickType(eventData));
            inventoryEventSo.OnInventoryClickEvent(_inventoryView);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            ShowOverlay(true);
            _inventoryView.SetCursor(_itemStack);
            _inventoryView.SetSlot(_slot);
            inventoryEventSo.OnInventoryItemSlotEnterEvent(_itemStack);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            ShowOverlay(false);
            _inventoryView.SetCursor(null);
            inventoryEventSo.OnInventoryItemSlotLeaveEvent();
        }

        public void Init(ItemStack itemStack, int slot, InventoryView inventoryView)
        {
            _itemStack = itemStack;
            _slot = slot;
            _inventoryView = inventoryView;
            ShowOverlay(false);

            if (itemStack == null) return;
            var itemAmount = itemStack.GetItemMeta().GetAmount();
            itemAmountText.text = itemAmount > 1 ? $"{itemAmount}" : string.Empty;
            slotIcon.sprite = itemIconPack.GetItemIcon(itemStack.GetType());
        }

        private void ShowOverlay(bool show)
        {
            hoverOverlay.gameObject.SetActive(show);
        }

        private InventoryAction GetAction(ItemStack itemStack)
        {
            return itemStack == null ? InventoryAction.NOTHING : InventoryAction.PICKUP;
        }

        private ClickType GetClickType(PointerEventData eventData)
        {
            switch (eventData.button)
            {
                case PointerEventData.InputButton.Left:
                    return ClickType.LEFT;
                case PointerEventData.InputButton.Right:
                    return ClickType.RIGHT;
                case PointerEventData.InputButton.Middle:
                    return ClickType.MIDDLE;
                default:
                    return ClickType.NONE;
            }
        }
    }
}