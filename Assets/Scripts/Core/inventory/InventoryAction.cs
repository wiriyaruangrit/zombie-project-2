﻿namespace Core.inventory
{
    public enum InventoryAction
    {
        NOTHING,
        SWAP_WITH_CURSOR,
        PICKUP
    }
}