﻿using Cinemachine;
using Core.armor;
using Core.entity.livingentity;
using Core.events;
using Core.spectator;
using UnityEngine;

namespace Core.entity
{
    public class LivingEntity : Entity, IDamageable, ISpectator
    {
        [Header("LIVING ENTITY")] [SerializeField]
        private CinemachineVirtualCamera virtualCamera;

        [SerializeField] private ArmorType armorType;
        [SerializeField] private float maxHealth;
        [Header("ATK")] [SerializeField] private float minAttack = 1;
        [SerializeField] private float maxAttack = 2;
        private float _absorption;

        private float _health;
        private ILivingEntityAnimation _livingEntityAnimation;

        private ILivingEntityAttackResponse _livingEntityAttackResponse;

        protected override void Awake()
        {
            base.Awake();
            _livingEntityAttackResponse = GetComponent<ILivingEntityAttackResponse>();
            _livingEntityAnimation = GetComponent<ILivingEntityAnimation>();

            _health = maxHealth;
        }

        public void Damage(float damage)
        {
            if (damage < 0) return;
            EventInstance.Instance.EntityEvent.OnEntityDamageEvent(this, damage);
            _health -= damage;

            if (_health > 0) return;
            _isDead = true;
            EventInstance.Instance.EntityEvent.OnEntityDeathEvent(this);
            Destroy(gameObject);
        }

        public void Damage(float damage, Entity source)
        {
            Damage(damage);
        }

        public void Damage(float damage, Entity source, bool isDamageModified)
        {
            Damage(damage);
            EventInstance.Instance.EntityEvent.OnEntityDamageModifiedEvent(this, damage, isDamageModified);
        }

        public float GetHealth()
        {
            return _health;
        }

        public void SetHealth(float health)
        {
            _health = health;
        }

        public float GetMaxHealth()
        {
            return maxHealth;
        }

        public void SetMaxHealth(float health)
        {
            maxHealth = health;
        }

        public float GetAbsorptionAmount()
        {
            return _absorption;
        }

        public void SetAbsorptionAmount(float amount)
        {
            _absorption = amount;
        }

        public ArmorType GetArmorType()
        {
            return armorType;
        }

        public Transform GetEyeTransform()
        {
            return virtualCamera.transform;
        }

        public void Attack(Entity target)
        {
            _livingEntityAttackResponse.Attack(this, target, Random.Range(minAttack, maxAttack));
        }

        public void SwingHands()
        {
            _livingEntityAnimation.SwingHands();
        }

        public CinemachineVirtualCamera GetCamera()
        {
            return virtualCamera;
        }
    }
}