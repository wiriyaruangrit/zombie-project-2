﻿using Core.armor;

namespace Core.entity
{
    public interface IDamageable
    {
        void Damage(float damage);
        void Damage(float damage, Entity source);
        void Damage(float damage, Entity source, bool isDamageModified);
        float GetHealth();
        void SetHealth(float health);
        float GetMaxHealth();
        void SetMaxHealth(float health);
        float GetAbsorptionAmount();
        void SetAbsorptionAmount(float amount);
        ArmorType GetArmorType();
    }
}