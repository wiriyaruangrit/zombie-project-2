﻿using UnityEngine;

namespace Core.entity
{
    public abstract class Entity : MonoBehaviour
    {
        [SerializeField] private EntityType entityType;

        protected bool _isDead = false;

        protected virtual void Awake()
        {
        }

        protected virtual void Update()
        {
        }


        protected virtual void FixedUpdate()
        {
        }

        public bool IsDead()
        {
            return _isDead;
        }

        public Vector3 GetLocation()
        {
            return transform.position;
        }

        public void SetLocation(Vector3 location)
        {
            transform.position = location;
        }

        public new EntityType GetType()
        {
            return entityType;
        }
    }
}