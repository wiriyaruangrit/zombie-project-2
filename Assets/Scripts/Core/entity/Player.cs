﻿using System.Linq;
using Core.events;
using UnityEngine;
using ZombieProject.game.wave;

namespace Core.entity
{
    public class Player : HumanEntity
    {
        [Header("PLAYER")] [SerializeField] private float colliderLength = 2;
        private IWaveSpawner _waveSpawner;

        protected override void Awake()
        {
            base.Awake();
            _waveSpawner = FindObjectOfType<WaveSpawner>();
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            CheckHitEnemy();
        }

        private void CheckHitEnemy()
        {
            foreach (var monster in _waveSpawner.GetEnemies().Where(monster =>
                !(Vector3.Distance(monster.GetLocation(), GetLocation()) > colliderLength)))
            {
                EventInstance.Instance.PlayerEvent.OnPlayerHitMonsterEvent(monster);
                break;
            }
        }
    }
}