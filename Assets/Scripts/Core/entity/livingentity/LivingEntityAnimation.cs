﻿using UnityEngine;

namespace Core.entity.livingentity
{
    public class LivingEntityAnimation : MonoBehaviour, ILivingEntityAnimation
    {
        [SerializeField] private Animator animator;

        public void SwingHands()
        {
            animator.SetTrigger("attack");
        }
    }
}