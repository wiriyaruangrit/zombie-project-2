﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Utils
{
    //  [ExecuteInEditMode]
    public class UIColorHelper : MonoBehaviour
    {
        [SerializeField] private ImageColorData[] imageColorData;

        private void OnEnable()
        {
            Debug.Log("RUN...");
            DoSomething();
        }

        private void DoSomething()
        {
            foreach (var data in imageColorData)
            foreach (var image in data.images)
                image.color = data.color;
        }

        [Serializable]
        public struct ImageColorData
        {
            public Image[] images;
            public Color color;
        }
    }
}