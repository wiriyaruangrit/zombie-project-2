﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Utils
{
    public class GameDebug : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI logText;
        [SerializeField] private TMP_InputField inputField;
        [SerializeField] private int limitLog;
        [SerializeField] private Transform consoleTransform;

        private const string CmdSymbol = "/";
        public Action<string> OnRunCommand;

        private readonly Queue<string> _cmdLogs = new Queue<string>();

        private void Awake()
        {
            inputField.onSubmit.AddListener(OnSubmit);
        }

        private void OnSubmit(string s)
        {
            if (string.IsNullOrEmpty(s)) return;
            inputField.text = string.Empty;

            if (s.StartsWith(CmdSymbol))
            {
                OnRunCommand?.Invoke(s.Replace(CmdSymbol, string.Empty));
                return;
            }

            AddLog(s);
            ShowLog();
            inputField.Select();
        }

        public void AddLog(string log)
        {
            _cmdLogs.Enqueue(log);
            if (_cmdLogs.Count > limitLog)
            {
                _cmdLogs.Dequeue();
            }

            ShowLog();
        }

        private void ShowLog()
        {
            logText.text = string.Empty;
            foreach (var cmd in _cmdLogs)
            {
                logText.text += $"{cmd}\n";
            }
        }

        private void ShowConsole()
        {
            consoleTransform.gameObject.SetActive(true);

            GameCursor.Show();
            inputField.Select();
        }

        private void HideConsole()
        {
            consoleTransform.gameObject.SetActive(false);
            GameCursor.Hide();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.BackQuote))
            {
                ShowConsole();
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                HideConsole();
            }
        }
    }
}